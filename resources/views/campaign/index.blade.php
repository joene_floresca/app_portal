@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">Campaign List</div>
				<div class="panel-body">
					<div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                          @if(Session::has('alert-' . $msg))
                          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                          @endif
                        @endforeach
                    </div>
                    <table id="CampaignList" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                            	<th colspan="2"> <center>Campaign Information<center></th>
                                <th colspan="2"> <center>Actions<center></th>
                            </tr>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
						</tfoot>
                    </table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('campaign_scripts')
    <script type="text/javascript">
    $.ajax({
        url: "campaign-list",
        type: 'GET',
        success: function(result){
        var myObj = $.parseJSON(result);
        $.each(myObj, function(key,value) {
            var t = $('#CampaignList').DataTable();
            t.row.add( [
                value.id,
                value.name,
                "<a class='btn btn-small btn-info' href='<?php echo URL::to('campaign').'/';?>"+value.id+"/edit'><span class='glyphicon glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
                "<form method='POST' action='<?php echo URL::to('campaign').'/';?>"+value.id+"' accept-charset='UTF-8' class='pull-left' >"+
                "<input name='_method' type='hidden' value='DELETE'>"+
                "<button type='submit' class='btn btn-warning'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button>"+"</form>",

            ] ).draw();
        });
    }});
    </script>
@endsection
