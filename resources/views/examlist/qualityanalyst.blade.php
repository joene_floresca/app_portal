@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Quality Analyst</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<h1>Please take the following exams: </h1>

					<ol>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=general-english-test">General English Test</a></li>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=quick-english-level-test">Quick English Test</a></li>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=excel-quiz-basic-knowledge-test">Spreadsheet Quiz 1 </a></li>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=microsoft-excel-quiz">Spreadsheet Quiz 2 </a></li>
					</ol>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection

