@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Graphic Designer</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>

					<h1>Please take the following exams: </h1>

					<ol>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=general-english-test">General English Test</a></li>
						<li><a target="_blank" href="http://www.proprofs.com/quiz-school/story.php?title=quick-english-level-test">Quick English Test</a></li>
					</ol>

					<iframe name='proprofs' id='proprofs' width='650' height='700' frameborder=0 marginwidth=0 marginheight=0 src='https://www.proprofs.com/quiz-school/story.php?title=general-english-test&id=107183&ew=630'></iframe><div style='font-size:10px; font-family:Arial, Helvetica, sans-serif; color:#000;text-align:center;'><a href='https://www.proprofs.com/quiz-school/story.php?title=general-english-test' target='_blank' title='ProProfs Quiz- General English Test'>ProProfs Quiz- General English Test</a></div>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection

