@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Application Form</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
			         <form class="form-horizontal" role="form" method="POST" action="{{ url('applicant') }}">
			         	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				      <fieldset>
					      <div class="tabbable">
					        <ul class="nav nav-tabs" id="tab_bar">
					          <li class="active"><a href="#tab1" data-toggle="tab"><i class="glyphicon glyphicon-pencil"></i> Application</a></li>
					          <li><a href="#tab2" data-toggle="tab"><i class="glyphicon glyphicon-user"></i> Personal Info</a></li>
					          <li><a href="#tab3" data-toggle="tab"><i class="glyphicon glyphicon-list-alt"></i> Educational Background</a></li>
					          <li><a href="#tab4" data-toggle="tab"><i class="glyphicon glyphicon-briefcase"></i> Employment Record</a></li>
					          <li><a href="#tab5" data-toggle="tab"><i class="glyphicon glyphicon-paperclip"></i> Character References</a></li>
					        </ul>
					        
					        <div class="tab-content">
					          <!-- TAB 1 -->
					          <div class="tab-pane active" id="tab1">
					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<select class="form-control" name="position_applied" id="position_applied">
											<option value="">Choose One</option>
											<option value="Outbound Sales Specialist">Outbound Sales Specialist</option>
											<option value="Outbound Survey Specialist">Outbound Survey Specialist</option>
											<option value="Virtual Assistant">Virtual Assistant</option>
											<option value="Customer Service Representative">Customer Service Representative</option>
											<option value="Tech Support Representative">Tech Support Representative</option>
											<option value="Outbound Agent">Outbound Agent</option>
											<option value="Quality Analyst">Quality Analyst</option>
											<option value="Reports Analyst">Reports Analyst</option>
											<option value="Team Leader">Team Leader</option>
											<option value="SEO Specialist">SEO Specialist</option>
											<option value="SEM / PPC / Adwords Specialist">SEM / PPC / Adwords Specialist</option>
											<option value="Front End Developer">Front End Developer</option>
											<option value="Back End Developer">Back End Developer</option>
											<option value="Graphics Designer">Graphics Designer</option>
											<option value="Bookkeeper">Bookkeeper</option>
											<option value="Trainer">Trainer</option>
											<option value="Operations Manager">Operations Manager</option>
											<option value="QA Verifier">QA Verifier</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Availability to start</label>
					                <div class="controls">
					                  	<select class="form-control" name="start_availability" id="start_availability">
											<option value="">Choose One</option>
											<option value="Asap">Asap</option>
											<option value="5 days">5 days</option>
											<option value="10 days">10 days</option>
											<option value="15 days">15 days</option>
											<option value="20 days">20 days</option>
											<option value="30 days">30 days</option>
											<option value="Others">Others</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Expected Salary</label>
					                <div class="controls">
					                  	<input class="form-control" name="expected_salary" id="expected_salary" />
					                </div>
					            </div>
					            

					           </div>
					          
					          <!-- TAB 2 -->
					          <div class="tab-pane pre-scrollable" id="tab2">

					            <div class="control-group">
					              <label class="control-label" for="id_title">First Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="first_name" id="first_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Middle Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="middle_name" id="middle_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Last Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="last_name" id="last_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Current Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="current_address" id="current_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Provincial Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="provincial_address" id="provincial_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mobile Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="mobile_number" id="mobile_number" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Landline Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="landline_number" id="landline_number" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Email Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="email_address" id="email_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Age</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="age" id="age" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Gender</label>
					                <div class="controls">
					                  	<select class="form-control" name="gender" id="gender">
											<option value="">Choose One</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Birthdate</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="birthdate" id="birthdate" placeholder="Format: YYYY-MM-DD Ex. 1993-01-01"/>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Place of birth</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="birth_place" id="birth_place" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Citizienship</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="citizenship" id="citizenship" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Civil Status</label>
					                <div class="controls">
					                  	<select class="form-control" name="civil_status" id="civil_status">
											<option value="">Choose One</option>
											<option value="Single">Single</option>
											<option value="Married">Married</option>
											<option value="Separated">Separated</option>
											<option value="Annulled">Annulled</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Languages</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="languages" id="languages" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Name of Spouse</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="spouse" id="spouse" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Father's Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="father_name" id="father_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mother's Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="mother_name" id="mother_name" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Parent's Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="parent_address" id="parent_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Person to be notified in case of emergency:</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="person_emergency" id="person_emergency" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_address" id="emergency_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Landline Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_landline" id="emergency_landline" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mobile Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_mobile" id="emergency_mobile" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Relationship</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_relationship" id="emergency_relationship" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 3 -->
					          <div class="tab-pane pre-scrollable" id="tab3">

					          	<div class="control-group">
					              <label class="control-label" for="id_title">College / University </label>
					                <div class="controls">
					                  	<input class="form-control" name="college_university" id="college_university" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Degree</label>
					                <div class="controls">
					                  	<input class="form-control" name="degree" id="degree" />
					                </div>
					            </div>

					            <!-- <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="college_address" id="college_address" />
					                </div>
					            </div> -->

					             <div class="control-group">
					              <label class="control-label" for="id_title">Vocational</label>
					                <div class="controls">
					                  	<input class="form-control" name="vocational" id="vocational" />
					                </div>
					            </div>

					           <!--  <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="vocational_address" id="vocational_address" />
					                </div>
					            </div> -->

					            <div class="control-group">
					              <label class="control-label" for="id_title">High School</label>
					                <div class="controls">
					                  	<input class="form-control" name="highschool" id="highschool" />
					                </div>
					            </div>

					           <!--  <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="highschool_address" id="highschool_address" />
					                </div>
					            </div> -->

					            <div class="control-group">
					              <label class="control-label" for="id_title">Elementary</label>
					                <div class="controls">
					                  	<input class="form-control" name="elementary" id="elementary" />
					                </div>
					            </div>

					            <!--  <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="elementary_address" id="elementary_address" />
					                </div>
					            </div>
 -->
					            <div class="control-group">
					              <label class="control-label" for="id_title">Special Skills</label>
					                <div class="controls">
					                  	<input class="form-control" name="special_skills" id="special_skills" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 4 -->
					          <div class="tab-pane pre-scrollable" id="tab4">

					          	<!-- Company 1 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(1) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1" id="company1" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_address" id="company1_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_department" id="company1_department" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_position" id="company1_position" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_salary" id="company1_salary" />
					                </div>
					            </div>

					            <!-- Company 2 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(2) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2" id="company2" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_address" id="company2_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_department" id="company2_department" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_position" id="company2_position" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_salary" id="company2_salary" />
					                </div>
					            </div>

					            <!-- Company 3 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(3) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3" id="company3" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_address" id="company3_address" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_department" id="company3_department" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_position" id="company3_position" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_salary" id="company3_salary" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 5 -->
					          <div class="tab-pane" id="tab5">
					          		<div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 1</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref1" id="characterref1" placeholder="Name, Company, Position, Mobile /Phone no." />
						                </div>
						            </div>
						            <div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 2</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref2" id="characterref2" placeholder="Name, Company, Position, Mobile /Phone no."/>
						                </div>
						            </div>
						            <div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 3</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref3" id="characterref3" placeholder="Name, Company, Position, Mobile /Phone no."/>
						                </div>
						            </div>

						             <div class="form-group">
						             	<hr class="border-line">
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">
												Submit
											</button>
										</div>
									</div>

					          </div>





					        </div>
					    <hr class="border-line"> <!-- STYLED IN FORMS.CSS -->
					   <!--  <div class="form-group">
							<div class="col-md-6">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div> -->
				      </fieldset>
				    </form>
					<div class="btn-group">
					    <button class="btn btn-warning" id="prevtab" type="button">Prev</button>
					    <button class="btn btn-warning" id="nexttab" type="button">Next</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection

