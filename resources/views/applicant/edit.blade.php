@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Application Form</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
			         {!! Form::model($applicant, array('route' => array('applicant.update', $applicant->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
			         	<input type="hidden" name="_token" value="{{ csrf_token() }}">
				      <fieldset>
					      <div class="tabbable">
					        <ul class="nav nav-tabs" id="tab_bar">
					          <li class="active"><a href="#tab1" data-toggle="tab"><i class="glyphicon glyphicon-pencil"></i> Application</a></li>
					          <li><a href="#tab2" data-toggle="tab"><i class="glyphicon glyphicon-user"></i> Personal Info</a></li>
					          <li><a href="#tab3" data-toggle="tab"><i class="glyphicon glyphicon-list-alt"></i> Educational Background</a></li>
					          <li><a href="#tab4" data-toggle="tab"><i class="glyphicon glyphicon-briefcase"></i> Employment Record</a></li>
					          <li><a href="#tab5" data-toggle="tab"><i class="glyphicon glyphicon-paperclip"></i> Character References</a></li>
					          <li><a href="#tab6" data-toggle="tab"><i class="glyphicon glyphicon-pushpin"></i> Application Status</a></li>
					        </ul>
					        
					        <div class="tab-content">
					          <!-- TAB 1 -->
					          <div class="tab-pane active" id="tab1">
					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                	{!! Form::select('position_applied', [
					                	'' => 'Choose One', 
					                	'Customer Service Representative' => 'Customer Service Representative', 
					                	'Tech Support Representative' => 'Tech Support Representative', 
					                	'Outbound Agent' => 'Outbound Agent', 
					                	'Quality Analyst' => 'Quality Analyst',
					                	'Reports Analyst' => 'Reports Analyst',
					                	'Team Leader' => 'Team Leader',
					                	'SEO Specialist' => 'SEO Specialist',
					                	'SEM / PPC / Adwords Specialist' => 'SEM / PPC / Adwords Specialist',
					                	'Front End Developer' => 'Front End Developer',
					                	'Back End Developer' => 'Back End Developer',
					                	'Graphics Designer' => 'Graphics Designer',
					                	'Bookkeeper' => 'Bookkeeper',
					                	'Trainer' => 'Trainer',
					                	'Operations Manager' => 'Operations Manager'
					                	], $applicant->position, array('class' => 'form-control')) !!}
					                  
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Availability to start</label>
					                <div class="controls">
					                	{!! Form::select('start_availability', [
					                	'' => 'Choose One', 
					                	'Asap' => 'Asap', 
					                	'5 days' => '5 days', 
					                	'10 days' => '10 days', 
					                	'15 days' => '15 days', 
					                	'20 days' => '20 days', 
					                	'30 days' => '30 days', 
					                	'Others' => 'Others'
					                	
					                	], $applicant->availability, array('class' => 'form-control')) !!}
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Expected Salary</label>
					                <div class="controls">
					                  	<input class="form-control" name="expected_salary" id="expected_salary" value="{{$applicant->expected_salary}}" />
					                </div>
					            </div>
					            

					           </div>
					          
					          <!-- TAB 2 -->
					          <div class="tab-pane pre-scrollable" id="tab2">

					            <div class="control-group">
					              <label class="control-label" for="id_title">First Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="first_name" id="first_name" value="{{$applicant->fname}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Middle Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="middle_name" id="middle_name" value="{{$applicant->mname}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Last Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="last_name" id="last_name" value="{{$applicant->lname}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Current Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="current_address" id="current_address" value="{{$applicant->current_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Provincial Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="provincial_address" id="provincial_address" value="{{$applicant->provincial_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mobile Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="mobile_number" id="mobile_number" value="{{$applicant->mobile_number}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Landline Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="landline_number" id="landline_number" value="{{$applicant->landline}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Email Address</label>
					                <div class="controls">
					                  	<input type="email" class="form-control" name="email_address" id="email_address" value="{{$applicant->email}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Age</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="age" id="age" value="{{$applicant->age}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Gender</label>
					                <div class="controls">
					                  	<select class="form-control" name="gender" id="gender">
											<option value="">Choose One</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Birthdate</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="birthdate" id="birthdate" value="{{$applicant->birthdate}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Place of birth</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="birth_place" id="birth_place" value="{{$applicant->birth_place}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Citizienship</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="citizenship" id="citizenship" value="{{$applicant->citizenship}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Civil Status</label>
					                <div class="controls">
					                	{!! Form::select('civil_status', [
					                	'' => 'Choose One', 
					                	'Single' => 'Single',
					                	'Married' => 'Married',
					                	'Separated' => 'Separated',
					                	'Annulled' => 'Annulled'
					                	], $applicant->civilstatus, array('class' => 'form-control')) !!}
					                  	
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Languages</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="languages" id="languages" value="{{$applicant->languages}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Name of Spouse</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="spouse" id="spouse" value="{{$applicant->spause}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Father's Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="father_name" id="father_name" value="{{$applicant->father_name}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mother's Name</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="mother_name" id="mother_name" value="{{$applicant->mother_name}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Parent's Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="parent_address" id="parent_address" value="{{$applicant->parent_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Person to be notified in case of emergency:</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="person_emergency" id="person_emergency" value="{{$applicant->person_emergency}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_address" id="emergency_address" value="{{$applicant->person_emergency_addresss}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Landline Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_landline" id="emergency_landline" value="{{$applicant->person_emergency_landline}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Mobile Number</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_mobile" id="emergency_mobile" value="{{$applicant->person_emergency_mobile}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Relationship</label>
					                <div class="controls">
					                  	<input type="text" class="form-control" name="emergency_relationship" id="emergency_relationship" value="{{$applicant->person_emergency_relationship}}" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 3 -->
					          <div class="tab-pane pre-scrollable" id="tab3">

					          	<div class="control-group">
					              <label class="control-label" for="id_title">College / University </label>
					                <div class="controls">
					                  	<input class="form-control" name="college_university" id="college_university" value="{{$applicant->college_university}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Degree</label>
					                <div class="controls">
					                  	<input class="form-control" name="degree" id="degree" value="{{$applicant->college_university_degree}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="college_address" id="college_address" value="{{$applicant->college_university_address}}" />
					                </div>
					            </div>

					             <div class="control-group">
					              <label class="control-label" for="id_title">Vocational</label>
					                <div class="controls">
					                  	<input class="form-control" name="vocational" id="vocational" value="{{$applicant->vocational}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="vocational_address" id="vocational_address" value="{{$applicant->vocational_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">High School</label>
					                <div class="controls">
					                  	<input class="form-control" name="highschool" id="highschool" value="{{$applicant->high_school}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="highschool_address" id="highschool_address"  value="{{$applicant->highschool_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Elementary</label>
					                <div class="controls">
					                  	<input class="form-control" name="elementary" id="elementary" value="{{$applicant->elementary}}" />
					                </div>
					            </div>

					             <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="elementary_address" id="elementary_address" value="{{$applicant->elementary_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Special Skills</label>
					                <div class="controls">
					                  	<input class="form-control" name="special_skills" id="special_skills" value="{{$applicant->special_skills}}" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 4 -->
					          <div class="tab-pane pre-scrollable" id="tab4">

					          	<!-- Company 1 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(1) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1" id="company1" value="{{$applicant->company1}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_address" id="company1_address" value="{{$applicant->company1_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_department" id="company1_department" value="{{$applicant->company1_department}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_position" id="company1_position" value="{{$applicant->company1_position}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company1_salary" id="company1_salary" value="{{$applicant->company1_salary}}" />
					                </div>
					            </div>

					            <!-- Company 2 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(2) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2" id="company2" value="{{$applicant->company2}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_address" id="company2_address" value="{{$applicant->company2_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_department" id="company2_department" value="{{$applicant->company2_department}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_position" id="company2_position" value="{{$applicant->company2_position}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company2_salary" id="company2_salary" value="{{$applicant->company2_salary}}" />
					                </div>
					            </div>

					            <!-- Company 3 -->

					          	<div class="control-group">
					              <label class="control-label" for="id_title">(3) Company Name</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3" id="company3" value="{{$applicant->company3}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Address</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_address" id="company3_address" value="{{$applicant->company3_address}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Account / Deparment</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_department" id="company3_department" value="{{$applicant->company3_department}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Position</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_position" id="company3_position" value="{{$applicant->company3_position}}" />
					                </div>
					            </div>

					            <div class="control-group">
					              <label class="control-label" for="id_title">Compensation</label>
					                <div class="controls">
					                  	<input class="form-control" name="company3_salary" id="company3_salary" value="{{$applicant->company3_salary}}" />
					                </div>
					            </div>

					          </div>

					          <!-- TAB 5 -->
					          <div class="tab-pane" id="tab5">
					          		<div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 1</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref1" id="characterref1" placeholder="Name, Company, Position, Mobile /Phone no." value="{{$applicant->character_ref1}}" />
						                </div>
						            </div>
						            <div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 2</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref2" id="characterref2" placeholder="Name, Company, Position, Mobile /Phone no." value="{{$applicant->character_ref2}}" />
						                </div>
						            </div>
						            <div class="control-group">
						              <label class="control-label" for="id_title">Character Reference 3</label>
						                <div class="controls">
						                  	<input class="form-control" name="characterref3" id="characterref3" placeholder="Name, Company, Position, Mobile /Phone no." value="{{$applicant->character_ref3}}" />
						                </div>
						            </div>
					          </div>

					          <!-- TAB 6 -->
					          <div class="tab-pane" id="tab6">
					          		<div class="control-group">
						              <label class="control-label" for="id_title">Application Status</label>
						                <div class="controls">
						                  	{!! Form::select('application_status', [
							                	'' => 'Choose One', 
							                	'Pending' => 'Pending',
							                	'Hired' => 'Hired',
							                	'Retracted' => 'Retracted',
							                	'No Show' => 'No Show',
							                	'Failed - Ineligible for Hire' => 'Failed - Ineligible for Hire',
							                	'Failed – Reapply in 30 -60 days' => 'Failed – Reapply in 30 -60 days'
							                	], $applicant->application_status, array('class' => 'form-control')) !!}
						                </div>
						            </div>
						            
					          </div>

					        </div>
					    <hr class="border-line"> <!-- STYLED IN FORMS.CSS -->
					    <div class="form-group">
							<div class="col-md-6">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
							</div>
						</div>
				      </fieldset>
				    {!! Form::close() !!}
					<div class="btn-group">
					    <button class="btn btn-warning" id="prevtab" type="button">Prev</button>
					    <button class="btn btn-warning" id="nexttab" type="button">Next</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


@endsection

