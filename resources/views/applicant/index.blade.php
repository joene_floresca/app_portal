@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">Applicant List</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="flash-message">
				        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
				          @if(Session::has('alert-' . $msg))
				          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
				          @endif
				        @endforeach
			        </div>
					
                    <table id="applicantsList" class="table table-striped table-bordered" cellspacing="0" width="100%" style="padding-top: 5px;">
                        <thead>
                            <tr> 
                            	<th colspan="8"> <center>Applicant Information<center></th>
                            </tr>
                            <tr>
                                <th>Application Status</th>
                                <th>Applicant Name</th>
                                <th>Position</th>
                                <th>Availability</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Application Date</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table> 

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('applicant_list')
<script type="text/javascript">
	$.ajax({
		url: "ajax-applicants", 
		type: 'GET',
		success: function(result){
		var myObj = $.parseJSON(result);
    	$.each(myObj, function(key,value) {
    		var t = $('#applicantsList').DataTable();
    		t.row.add( [
    			value.application_status,
	            value.fname + " " +  value.mname + " " + value.lname,
	            value.position,
	            value.availability,
	            value.mobile_number,
	            value.email,
	            value.created_at,
	            "<a class='btn btn-small btn-info' href='<?php echo URL::to('applicant').'/';?>"+value.id+"/edit'><span class='glyphicon glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
        	] ).draw();
		});
	}});
	</script>
@endsection

