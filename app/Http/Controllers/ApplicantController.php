<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Applicant;
use Validator;
use Input;
use Redirect;
use Session;
use View;
use Auth;

class ApplicantController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->middleware('auth');
        return view('applicant.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->middleware('auth');
        return view('applicant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'position_applied'          => 'required',
            'start_availability'        => 'required',
            'expected_salary'           => 'required',
            'first_name'                => 'required',
            'middle_name'               => 'required',
            'last_name'                 => 'required',
            'current_address'           => 'required',
           // 'provincial_address'        => 'required',
            'mobile_number'             => 'required',
           // 'landline_number'           => 'required',
            'email_address'             => 'required',
            'age'                       => 'required',
            'gender'                    => 'required',
            'birthdate'                 => 'required',
            // 'birth_place'               => 'required',
            // 'citizenship'               => 'required',
            // 'civil_status'              => 'required',
            // 'languages'                 => 'required',
            // 'father_name'               => 'required',
            // 'mother_name'               => 'required',
            // 'parent_address'            => 'required',
            // 'person_emergency'          => 'required',
            // 'emergency_address'         => 'required',
            // 'emergency_landline'        => 'required',
            // 'emergency_mobile'          => 'required',
            // 'emergency_relationship'    => 'required',
            // 'college_university'        => 'required',
            // 'degree'                    => 'required',
            // 'college_address'           => 'required',
            // 'vocational'                => 'required',
            // 'vocational_address'        => 'required',
            // 'highschool'                => 'required',
            // 'highschool_address'        => 'required',
            // 'elementary'                => 'required',
            // 'elementary_address'        => 'required',
            // 'special_skills'            => 'required',
            // 'characterref1'             => 'required',
            // 'characterref2'             => 'required',
            // 'characterref3'             => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) 
        {
            return Redirect::to('applicant/create')->withErrors($validator);
        }
        else
        {
            $applicants = new Applicant;
            $applicants->position                                 = Input::get('position_applied');
            $applicants->availability                             = Input::get('start_availability');
            $applicants->expected_salary                          = Input::get('expected_salary');
            $applicants->fname                                    = Input::get('first_name');
            $applicants->mname                                    = Input::get('middle_name');
            $applicants->lname                                    = Input::get('last_name');
            $applicants->current_address                          = Input::get('current_address');
            $applicants->provincial_address                       = Input::get('provincial_address');
            $applicants->mobile_number                            = Input::get('mobile_number');
            $applicants->landline                                 = Input::get('landline_number');
            $applicants->email                                    = Input::get('email_address');
            $applicants->age                                      = Input::get('age');
            $applicants->gender                                   = Input::get('gender');
            $applicants->birthdate                                = Input::get('birthdate');
            $applicants->birth_place                              = Input::get('birth_place');
            $applicants->citizenship                              = Input::get('citizenship');
            $applicants->civilstatus                              = Input::get('civil_status');
            $applicants->languages                                = Input::get('languages');
            $applicants->spause                                   = Input::get('spouse');
            $applicants->father_name                              = Input::get('father_name');
            $applicants->mother_name                              = Input::get('mother_name');
            $applicants->parent_address                           = Input::get('parent_address');
            $applicants->person_emergency                         = Input::get('person_emergency');
            $applicants->person_emergency_addresss                = Input::get('emergency_address');
            $applicants->person_emergency_landline                = Input::get('emergency_landline');
            $applicants->person_emergency_mobile                  = Input::get('emergency_mobile');
            $applicants->person_emergency_relationship            = Input::get('emergency_relationship');
            $applicants->college_university                       = Input::get('college_university');
            $applicants->college_university_degree                = Input::get('degree');
            $applicants->college_university_address               = Input::get('college_address');
            $applicants->vocational                               = Input::get('vocational');
            $applicants->vocational_address                       = Input::get('vocational_address');
            $applicants->high_school                              = Input::get('highschool');
            $applicants->highschool_address                       = Input::get('highschool_address');
            $applicants->elementary                               = Input::get('elementary');
            $applicants->elementary_address                       = Input::get('elementary_address');
            $applicants->special_skills                           = Input::get('special_skills');
            $applicants->company1                                 = Input::get('company1');
            $applicants->company1_address                         = Input::get('company1_address');
            $applicants->company1_department                      = Input::get('company1_department');
            $applicants->company1_position                        = Input::get('company1_position');
            $applicants->company1_salary                          = Input::get('company1_salary');
            $applicants->company2_address                         = Input::get('company2');
            $applicants->company2_address                         = Input::get('company2_address');
            $applicants->company2_department                      = Input::get('company2_department');
            $applicants->company2_position                        = Input::get('company2_position');
            $applicants->company2_salary                          = Input::get('company2_salary');
            $applicants->company3                                 = Input::get('company3');
            $applicants->company3_address                         = Input::get('company3_address');
            $applicants->company3_department                      = Input::get('company3_department');
            $applicants->company3_position                        = Input::get('company3_position');
            $applicants->company3_salary                          = Input::get('company3_salary');
            $applicants->character_ref1                           = Input::get('characterref1');
            $applicants->character_ref2                           = Input::get('characterref2');
            $applicants->character_ref3                           = Input::get('characterref3');

            if($applicants->save())
            {
                Session::flash('alert-success', 'Form Submitted Successfully.');
            }
            else
            {
                Session::flash('alert-danger', 'Form Submitted Successfully.');
            }

            switch(Input::get('position_applied'))
            {
                case 'Outbound Sales Specialist':
                    return Redirect::to('/exams/outboundsales');
                    break;
                case 'Graphics Designer':
                    return Redirect::to('/exams/graphicartist');
                    break;
                case 'Outbound Sales Specialist':
                    return Redirect::to('/exams/outboundsalesagent');
                    break;
                case 'Quality Analyst':
                    return Redirect::to('/exams/qualityanalyst');
                    break;      
                case 'Outbound Agent':
                    return Redirect::to('/exams/outboundagent');
                    break;        
                case 'Outbound Survey Specialist':
                    return Redirect::to('/exams/outboundsurvey');
                    break;  
                case 'Tech Support Representative':
                    return Redirect::to('/exams/techsupportrep');
                    break;    
                case 'Customer Service Representative':
                    return Redirect::to('/exams/customerservicerep');
                    break;  
                case 'SEO Specialist':
                    return Redirect::to('/exams/seogeneral');
                    break;
                case 'SEM / PPC / Adwords Specialist':
                    return Redirect::to('/exams/seogeneral');
                    break;  
                 case 'Team Leader':
                    return Redirect::to('/exams/teamlead');
                    break;     
                case 'Virtual Assistant':
                    return Redirect::to('/exams/virtualassistant');
                case 'QA Verifier':
                    return Redirect::to('/exams/qaverifier');    
                    break;          

            }   
            

            return Redirect::to('applicant/create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if(Auth::check())
        {
            $applicant = Applicant::find($id);
            return View::make('applicant.edit')->with('applicant', $applicant);
        }
        else
        {
            return View::make('errors.404');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $applicants = Applicant::find($id);
            $applicants->position                                 = Input::get('position_applied');
            $applicants->availability                             = Input::get('start_availability');
            $applicants->expected_salary                          = Input::get('expected_salary');
            $applicants->fname                                    = Input::get('first_name');
            $applicants->mname                                    = Input::get('middle_name');
            $applicants->lname                                    = Input::get('last_name');
            $applicants->current_address                          = Input::get('current_address');
            $applicants->provincial_address                       = Input::get('provincial_address');
            $applicants->mobile_number                            = Input::get('mobile_number');
            $applicants->landline                                 = Input::get('landline_number');
            $applicants->email                                    = Input::get('email_address');
            $applicants->age                                      = Input::get('age');
            $applicants->gender                                   = Input::get('gender');
            $applicants->birthdate                                = Input::get('birthdate');
            $applicants->citizenship                              = Input::get('citizenship');
            $applicants->civilstatus                              = Input::get('civil_status');
            $applicants->languages                                = Input::get('languages');
            $applicants->spause                                   = Input::get('spouse');
            $applicants->father_name                              = Input::get('father_name');
            $applicants->mother_name                              = Input::get('mother_name');
            $applicants->parent_address                           = Input::get('parent_address');
            $applicants->person_emergency                         = Input::get('person_emergency');
            $applicants->person_emergency_addresss                = Input::get('emergency_address');
            $applicants->person_emergency_landline                = Input::get('emergency_landline');
            $applicants->person_emergency_mobile                  = Input::get('emergency_mobile');
            $applicants->person_emergency_relationship            = Input::get('emergency_relationship');
            $applicants->college_university                       = Input::get('college_university');
            $applicants->college_university_degree                = Input::get('degree');
            $applicants->college_university_address               = Input::get('college_address');
            $applicants->vocational                               = Input::get('vocational');
            $applicants->vocational_address                       = Input::get('vocational_address');
            $applicants->high_school                              = Input::get('highschool');
            $applicants->highschool_address                       = Input::get('highschool_address');
            $applicants->elementary                               = Input::get('elementary');
            $applicants->elementary_address                       = Input::get('elementary_address');
            $applicants->special_skills                           = Input::get('special_skills');
            $applicants->company1                                 = Input::get('company1');
            $applicants->company1_address                         = Input::get('company1_address');
            $applicants->company1_department                      = Input::get('company1_department');
            $applicants->company1_position                        = Input::get('company1_position');
            $applicants->company1_salary                          = Input::get('company1_salary');
            $applicants->company2_address                         = Input::get('company2');
            $applicants->company2_address                         = Input::get('company2_address');
            $applicants->company2_department                      = Input::get('company2_department');
            $applicants->company2_position                        = Input::get('company2_position');
            $applicants->company2_salary                          = Input::get('company2_salary');
            $applicants->company3                                 = Input::get('company3');
            $applicants->company3_address                         = Input::get('company3_address');
            $applicants->company3_department                      = Input::get('company3_department');
            $applicants->company3_position                        = Input::get('company3_position');
            $applicants->company3_salary                          = Input::get('company3_salary');
            $applicants->character_ref1                           = Input::get('characterref1');
            $applicants->character_ref2                           = Input::get('characterref2');
            $applicants->character_ref3                           = Input::get('characterref3');
            $applicants->application_status                       = Input::get('application_status');

            if($applicants->save())
            {
                Session::flash('alert-success', 'Applicant Updated Successfully.');
            }
            else
            {
                Session::flash('alert-danger', 'Applicant Updated Successfully.');
            }
            

            return Redirect::to('applicant');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxGetApplicants()
    {
        return json_encode(Applicant::all());
    }

}
