<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Campaign;
use Validator;
use Session;
use Redirect;
use Input;
use Auth;
use Response;


class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin == 1)
        {
            return view('campaign.index');
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isAdmin == 1)
        {
            return view('campaign.create');
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|unique:campaigns',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('campaign/create')->withErrors($validator);
        }
        else
        {
            $campaign = new Campaign;
            $campaign->name = Input::get('name');

            if($campaign->save())
            {
                Session::flash('alert-success', 'Form Submitted Successfully.');
            }
            else
            {
                Session::flash('alert-danger', 'Failed to submit. Please try again.');
            }

            return Redirect::to('campaign/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isAdmin == 1)
        {
            $campaign = Campaign::find($id);
            return view('campaign.edit')->with('campaign', $campaign);
        }
        else
        {
            return Response::view('errors.404', array(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required|unique:campaigns',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return Redirect::to('campaign/create')->withErrors($validator);
        }
        else
        {
            $campaign = Campaign::find($id);
            $campaign->name = Input::get('name');

            if($campaign->save())
            {
                Session::flash('alert-success', 'Form Submitted Successfully.');
            }
            else
            {
                Session::flash('alert-danger', 'Failed to submit. Please try again.');
            }

            return Redirect::to('campaign/create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $campaign->delete();

        Session::flash('alert-success', 'Successfully deleted the campaign!');
        return Redirect::to('campaign');
    }

    public function getCampaignList()
    {
        return json_encode(Campaign::all());
    }
}
