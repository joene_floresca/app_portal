<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
});

/* Resource Routes */
Route::resource('applicant', 'ApplicantController');
Route::resource('campaign', 'CampaignController');

/* Ajax Routes */
Route::get('campaign-list', 'CampaignController@getCampaignList');
Route::get('ajax-applicants', 'ApplicantController@ajaxGetApplicants');

/* Exam Routes */
Route::get('/exams/outboundsales', function () {
    return view('examlist.outboundsales');
});

Route::get('/exams/qaverifier', function () {
    return view('examlist.qaverifier');
});

Route::get('/exams/outboundsurvey', function () {
    return view('examlist.outboundsurvey');
});

Route::get('/exams/techsupportrep', function () {
    return view('examlist.techsupportrep');
});

Route::get('/exams/customerservicerep', function () {
    return view('examlist.customerservicerep');
});

Route::get('/exams/seogeneral', function () {
    return view('examlist.seogeneral');
});

Route::get('/exams/virtualassistant', function () {
    return view('examlist.virtualassistant');
});

Route::get('/exams/graphicartist', function () {
    return view('examlist.graphicartist');
});

Route::get('/exams/outboundsalesagent', function () {
    return view('examlist.outboundsalesagent');
});

Route::get('/exams/outboundagent', function () {
    return view('examlist.outboundagent');
});

Route::get('/exams/qualityanalyst', function () {
    return view('examlist.qualityanalyst');
});

Route::get('/exams/teamlead', function () {
    return view('examlist.teamlead');
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
